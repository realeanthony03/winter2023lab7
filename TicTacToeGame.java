import java.util.Scanner;
public class TicTacToeGame
{

	public static void main(String[]args)
	{
		Scanner scan= new Scanner(System.in);
		System.out.println("Welcome to virtual tic tac toe!!");
		Board b = new Board();
		boolean gameOver=false;
		int player= 1;
		Square playerToken=Square.X;
		
		while(gameOver!=true)
		{
			System.out.println(b);
			if(player==1)
			{
				playerToken=Square.X;
			}
			else
			{
				playerToken=Square.O;
			}
			System.out.println("enter row");
			int row=scan.nextInt();
			System.out.println("enter column");
			int col=scan.nextInt();
			while(!b.placeToken(row,col,playerToken))
			{
			System.out.println("re-enter inputs");
			System.out.println("enter row");
			row=scan.nextInt();
			System.out.println("enter column");
			col=scan.nextInt();

			}
			
			if(b.checkIfWinning(playerToken))
			{
				System.out.println(b);
				System.out.println("Player "+player+ " is a Winner!");
				gameOver=true;
			}
			else
			{
				if(b.checkIfFull())
				{
					System.out.println("Its a Tie!");
					gameOver=true;
				}
				player+=1;
				if (player>2)
				{
					player=1;
				}
			}
			
		}
	}
}
public class Board
{
	private Square[][] tictactoeBoard ;
	
	public Board()
	{
		this.tictactoeBoard = new Square[3][3];
		for(int i =0; i< tictactoeBoard.length; i++)
		{
			for (int j =0; j<tictactoeBoard[i].length; j++)
			{
				this.tictactoeBoard[i][j]=Square.BLANK;
			}
		}
	}
	
	public String toString()
	{
		
		String completeBoard="  0  1  2";
		
		for (int i=0; i<tictactoeBoard.length;i++)
		{
			completeBoard+= "\n"+i +" ";

			for(int j=0;j<tictactoeBoard[i].length; j++)
			{
			 completeBoard+=tictactoeBoard[i][j] + " ";
			}	
		}
		return completeBoard;
	}
	
	public boolean placeToken(int row,int col, Square playerToken)
	{
		if (row>2 || col>2)
		{
			return false;
		}
		
		if(this.tictactoeBoard[row][col]!= Square.BLANK)
		{
			return false;
		}
		else
		{
			tictactoeBoard[row][col]=playerToken;

		}
		
		
		return true;
	}
	
	public boolean checkIfFull()
	{
		for(int i =0; i<tictactoeBoard.length; i++)
		{
			for (int j =0; j<tictactoeBoard[i].length; j++)
			{
				if(tictactoeBoard[i][j]==Square.BLANK)
				{
					return false;
				}
				
			}
			
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		int count=0;
		for(int row =0; row<this.tictactoeBoard.length; row++)
		{
		count=0;
			for (int col =0; col<this.tictactoeBoard[row].length; col++)
			{
				if (this.tictactoeBoard[row][col]==playerToken)
				{
					count+=1;
				}
				if(count==3)
				{	
				return true;
				}
			}
		}
		return false;
	}	
	
	private boolean checkIfWinningVertical(Square playerToken)
	{
		int count=0;
		for(int col =0; col<this.tictactoeBoard.length; col++)
		{
		count=0;
			for (int row =0; row<this.tictactoeBoard[col].length; row++)
			{
				if (this.tictactoeBoard[row][col]==playerToken)
				{
					count+=1;
				}
				if(count==3)
				{	
				return true;
				}
			}
		}
		return false;
	}	
	
	public boolean checkIfWinning(Square playerToken)
	{
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

}